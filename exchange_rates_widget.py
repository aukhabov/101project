# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait as wdw
from xml.dom.minidom import *
import time

class Test_Sberbank():

    def setup(self):
        xml_vars = parse("vars.xml")
        url = xml_vars.getElementsByTagName("url")[0].firstChild.nodeValue
        self.driver = webdriver.Chrome()
        self.driver.get(url)

    def teardown(self):
        self.driver.quit()

    def test_sb1_widget_container(self):
        d = self.driver
        assert d.find_element_by_css_selector('div.bp-container.sbt-springboard-container').is_displayed(), \
            'Widget container is not displayed on page'

    def test_sb2_exchange_widget(self):
        d = self.driver
        widget_titles = d.find_elements_by_class_name('sbt-springboard-area-holder')
        check = False
        for e in widget_titles:
            title = e.find_element_by_class_name('personalized-widget-title')
            if title.text == u'Курсы':
                check = True
                break
        assert check == True, 'Exchanges rate widget was not found in default widgets'

    def test_sb8_setting_buttons(self):
        d = self.driver
        widget_list = d.find_elements_by_class_name('sbt-springboard-area-holder')
        buttons = ['Catalog', 'Minimize', 'Remove', 'Setting']
        for e in widget_list:
            title = e.find_element_by_class_name('personalized-widget-title')
            if title.text == u'Курсы':
                e.find_element_by_class_name('widget-icon-hider').click()
                if e.find_element_by_class_name('remove-w').is_displayed():
                    buttons.remove('Remove')
                if e.find_element_by_class_name('minimize-w').is_displayed():
                    buttons.remove('Minimize')
                if e.find_element_by_class_name('catalog-w').is_displayed():
                    buttons.remove('Catalog')
                if e.find_element_by_class_name('widget-icon-hider').is_displayed():
                    buttons.remove('Setting')
                break
        assert buttons == [], "Buttons that wasn't displayed after click on Setting button: %s"%(buttons)

    def test_sb10_catalog_button(self):
        d = self.driver
        widget_list = d.find_elements_by_class_name('sbt-springboard-area-holder')
        check = False
        for e in widget_list:
            title = e.find_element_by_class_name('personalized-widget-title')
            if title.text == u'Курсы':
                check = True
                e.find_element_by_class_name('widget-icon-hider').click()
                e.find_element_by_class_name('catalog-w').click()
                wdw(d, 3).until(lambda d: e.find_element_by_css_selector('div.sbt-springboard-area.back'). \
                                find_element_by_class_name('sbrf-widget-catalog').is_displayed(),
                                message="Widget catalog wasn't displayed after click on catalog button "
                                        "in widget settings")
                break
        assert check == True, "Exchange rates widget wasn't found on page"

    def test_sb11_available_widgets_in_widget_catalog(self):
        d = self.driver
        widget_list = d.find_elements_by_class_name('sbt-springboard-area-holder')
        check = False
        enabledWidgets = []
        for e in widget_list:
            enabledWidgets.append(e.find_element_by_class_name('personalized-widget-title').text)
        for e in widget_list:
            title = e.find_element_by_class_name('personalized-widget-title')
            if title.text == u'Курсы':
                check = True
                e.find_element_by_class_name('widget-icon-hider').click()
                e.find_element_by_class_name('catalog-w').click()
                wdw(d, 3).until(lambda d: e.find_element_by_css_selector('div.sbt-springboard-area.back'). \
                                find_element_by_class_name('sbrf-widget-catalog').is_displayed(),
                                message="Widget catalog wasn't displayed after click on catalog button "
                                        "in widget settings")
                catalog = e.find_elements_by_class_name('sbrf-widget-catalog-item')
                for widget in catalog:
                    widgetClass = widget.get_attribute('class')
                    if widget.text in enabledWidgets:
                        assert 'selectable' not in widgetClass, "We can choose enabled widget " \
                                                                "in widget catalog: %s"%(widget.text)
                    else:
                        assert 'selectable' in widgetClass, "We can't choose disabled widget " \
                                                            "from widget catalog: %s"%(widget.text)
                break
        assert check == True, "Exchange rates widget wasn't found on page"

    def test_sb12_changing_widget_with_widget_catalog(self):
        d = self.driver
        widget_list = d.find_elements_by_class_name('sbt-springboard-area-holder')
        check = False
        enabledWidgets = []
        newWidgetTitle = u'Курсы'
        for e in widget_list:
            enabledWidgets.append(e.find_element_by_class_name('personalized-widget-title').text)
        for e in widget_list:
            title = e.find_element_by_class_name('personalized-widget-title')
            if title.text == u'Курсы':
                check = True
                e.find_element_by_class_name('widget-icon-hider').click()
                e.find_element_by_class_name('catalog-w').click()
                wdw(d, 3).until(lambda d: e.find_element_by_css_selector('div.sbt-springboard-area.back'). \
                                find_element_by_class_name('sbrf-widget-catalog').is_displayed(),
                                message="Widget catalog wasn't displayed after click on catalog button "
                                        "in widget settings")
                catalog = e.find_elements_by_class_name('sbrf-widget-catalog-item')
                assert e.find_element_by_class_name('personalized-widget-title').is_displayed()
                for widget in catalog:
                    if widget.text not in enabledWidgets:
                        newWidgetTitle = widget.text
                        widget.click()
                        time.sleep(1)
                        break
                actualWidgetTitle = e.find_element_by_class_name('personalized-widget-title').text
                assert newWidgetTitle == actualWidgetTitle, 'Incorrect widget title after change.' \
                                                            '\nCorrect widget title: %s' \
                                                            '\nActual widget title: %s'%(newWidgetTitle,
                                                                                            actualWidgetTitle)
                break
        assert check == True, "Exchange rates widget wasn't found on page"