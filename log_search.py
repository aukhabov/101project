# -*- coding: utf-8 -*-

import paramiko

def auth_data(user_ip):
    return {'login': 'userLogin', 'password': 'userPassword'}

# Получение ip-адреса
print 'Введите первый октет вашего ip-адреса ХХХ.ххх.ххх.ххх :'
while True:
    o1 = raw_input()
    if o1.isdigit():
        if 0 <= int(o1) <= 255:
            break
        else:
            print 'Величина октета не попадает в промежуток от 0 до 255.' \
                  '\nПожалуйста, введите первый октет снова:'
    else:
        print 'Введен некорректный тип данных. Нужно вводить целое число от 0 до 255.' \
              '\nПожалуйста, введите первый октет снова:'

print 'Введите второй октет вашего ip-адреса ххх.ХХХ.ххх.ххх :'
while True:
    o2 = raw_input()
    if o2.isdigit():
        if 0 <= int(o2) <= 255:
            break
        else:
            print 'Величина октета не попадает в промежуток от 0 до 255.' \
                  '\nПожалуйста, введите второй октет снова:'
    else:
        print 'Введен некорректный тип данных. Нужно вводить целое число от 0 до 255.' \
              '\nПожалуйста, введите второй октет снова:'

print 'Введите третий октет вашего ip-адреса ххх.ххх.ХХХ.ххх :'
while True:
    o3 = raw_input()
    if o3.isdigit():
        if 0 <= int(o3) <= 255:
            break
        else:
            print 'Величина октета не попадает в промежуток от 0 до 255.' \
                  '\nПожалуйста, введите третий октет снова:'
    else:
        print 'Введен некорректный тип данных. Нужно вводить целое число от 0 до 255.' \
              '\nПожалуйста, введите третий октет снова:'

print 'Введите четвертый октет вашего ip-адреса ххх.ххх.ххх.ХХХ :'
while True:
    o4 = raw_input()
    if o4.isdigit():
        if 0 <= int(o4) <= 255:
            break
        else:
            print 'Величина октета не попадает в промежуток от 0 до 255.' \
                  '\nПожалуйста, введите четвертый октет снова:'
    else:
        print 'Введен некорректный тип данных. Нужно вводить целое число от 0 до 255.' \
              '\nПожалуйста, введите четвертый октет снова:'

ip = '%s.%s.%s.%s'%(o1,o2,o3,o4)
print 'Ваш ip-адрес: %s\n'%(ip)

# Получение названия лога
print 'Введите название лога:'
log_name = raw_input()

# Получение числового идентификатора
print 'Введите числовой идентификатор для поиска в логе:'
while True:
    id = raw_input()
    if id.isdigit():
        break
    else:
        print 'Введен некорректный тип данных. Нужно ввести целое числовое значение.' \
              '\nПожалуйста, введите идентификатор снова:'

# Сам поиск
authorization = auth_data(ip)
client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect(hostname='server', username=authorization['login'], password=authorization['password'], port=22)
stdin, stdout, stderr = client.exec_command("grep '%s' /var/log/%s.log -B 100 -A 100"%(id, log_name))
data = stdout.read()
print data
client.close()
